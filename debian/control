Source: python-opentimestamps
Section: python
Priority: optional
Maintainer: Hanno Stock <opensource@hanno-stock.de>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 13), dh-python, python3-all, python3-setuptools
Standards-Version: 4.6.1
Homepage: https://github.com/opentimestamps/python-opentimestamps
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-opentimestamps
Vcs-Git: https://salsa.debian.org/python-team/packages/python-opentimestamps.git
Testsuite: autopkgtest-pkg-python

Package: python3-opentimestamps
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Python3 library for creating and verifying OpenTimestamps proofs
 OpenTimestamps aims to be a standard format for blockchain timestamping.
 The format is flexible enough to be vendor and blockchain independent.
 .
 A timestamp proves that some data existed prior to some point in time.
 OpenTimestamps defines a set of operations for creating provable timestamps and
 later independently verifying them.
 .
 This package installs the library for Python 3.
